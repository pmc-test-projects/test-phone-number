package com.paulmark.testphonenumber

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import io.michaelrocks.libphonenumber.android.Phonenumber
import java.util.*

class MainActivity : Activity() {

    var tvCountry: TextView? = null
    var etPhoneNumber: TextView? = null

    var countryLocale = Locale.getDefault().country

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvCountry = findViewById<TextView>(R.id.country)
        etPhoneNumber = findViewById<TextView>(R.id.phoneNumber)

        tvCountry!!.text = "Country: $countryLocale"

        val btFormat = findViewById<Button>(R.id.format)
        btFormat.setOnClickListener(View.OnClickListener {
            formatNumber(etPhoneNumber!!.text.toString())
        })
    }

    override fun onResume() {
        super.onResume()
        formatNumber(etPhoneNumber!!.text.toString())
    }

    fun formatNumber(number: String) {
        val tvFormattedPhoneNumber = findViewById<TextView>(R.id.formattedPhoneNumber)

        tvFormattedPhoneNumber.text = ""

        try {
            val util = PhoneNumberUtil.createInstance(this)
            val phoneNumberObject: Phonenumber.PhoneNumber = util.parse(number, countryLocale)

            if (util.isValidNumber(phoneNumberObject)) {
                val formattedNumber = util.format(phoneNumberObject, PhoneNumberUtil.PhoneNumberFormat.E164)
                tvFormattedPhoneNumber.text = "Number: $formattedNumber"
            } else {
                tvFormattedPhoneNumber.text = number
            }
        } catch (e: Exception) {
            tvFormattedPhoneNumber.text = e.message
        }
    }
}
